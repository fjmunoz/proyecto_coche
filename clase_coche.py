class Coche:
    def __init__(self, color, marca, modelo, matricula, tiempo, velocidad, aceleracion):
        self.__color = color
        self.__marca = marca
        self.__modelo = modelo
        self.__matricula = matricula
        self.__tiempo = tiempo
        self.__velocidad = velocidad
        self.__aceleracion = aceleracion
    def Acelerar(self):
        self.__velocidad = self.__velocidad + self.__aceleracion * self.__tiempo
        print("Tras {time} segundos y una aceleracion de {aceleration} metros por segundo al cuadrado, la velocidad ha aumentado a {speed:.2f} metros".format(time=self.__tiempo, aceleration=self.__aceleracion, speed=self.__velocidad))
        return self.__velocidad
    def Frenar(self): # Tomamos la frenada como la una desaceleración producida por la resistencia de las ruedas al movimineto y el rozamiento entre ruedas y pavimento
        self.__velocidad = self.__velocidad - self.__aceleracion * self.__tiempo
        if (self.__velocidad <= 0):
            self.__velocidad = 0
            print("El coche ha frenado completamente")
            return self.__velocidad
        else:
            print("Tras {time} segundos y una desaceleracion de {aceleration} metros por segundo al cuadrado, la velocidad ha disminuido a {speed:.2f} metros".format(time=self.__tiempo, aceleration=self.__aceleracion, speed=self.__velocidad))
            return self.__velocidad