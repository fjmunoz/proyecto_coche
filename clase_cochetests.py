
from clase_coche import Coche
import unittest

class TestCoche(unittest.TestCase):
    def setUp(self):
        self.coche = Coche("Rojo", "Toyota", "Corolla", "ABC123", 5, 20, 3)

    def test_acelerar(self):
        self.assertEqual(self.coche.Acelerar(), 35)

    def test_frenar(self):
        self.assertEqual(self.coche.Frenar(), 5)

    def test_frenar_completamente(self):
        self.coche._Coche__velocidad = 2
        self.assertEqual(self.coche.Frenar(), 0)

if __name__ == "__main__":
    unittest.main()